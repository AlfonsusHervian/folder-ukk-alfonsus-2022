USE [master]
GO
/****** Object:  Database [db_UKK_P2_Alfonsus]    Script Date: 5/27/2022 11:14:52 AM ******/
CREATE DATABASE [db_UKK_P2_Alfonsus]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Pemesanan_hotel2', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Pemesanan_hotel2.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Pemesanan_hotel2_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Pemesanan_hotel2_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_UKK_P2_Alfonsus].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET  MULTI_USER 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET DELAYED_DURABILITY = DISABLED 
GO
USE [db_UKK_P2_Alfonsus]
GO
/****** Object:  Table [dbo].[TBL_CEKIN]    Script Date: 5/27/2022 11:14:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CEKIN](
	[IDKamar] [varchar](5) NULL,
	[Lantai] [varchar](50) NULL,
	[NoKamar] [varchar](50) NULL,
	[TipeKamar] [varchar](50) NULL,
	[Fasilitas] [varchar](50) NULL,
	[Harga] [char](12) NULL,
	[KTP] [varchar](30) NOT NULL,
	[Nama] [varchar](50) NULL,
	[JenisKelamin] [varchar](50) NULL,
	[Alamat] [varchar](100) NULL,
	[NoTelp] [varchar](50) NULL,
	[TglCekin] [date] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CEKOUT]    Script Date: 5/27/2022 11:14:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CEKOUT](
	[KTP] [varchar](30) NOT NULL,
	[Nama] [varchar](50) NULL,
	[JenisKelamin] [varchar](50) NULL,
	[Alamat] [varchar](100) NULL,
	[NoTelp] [varchar](50) NULL,
	[TglCekin] [datetime] NULL,
	[TglCekout] [datetime] NULL,
	[IDKamar] [varchar](5) NULL,
	[Lantai] [varchar](50) NULL,
	[NoKamar] [varchar](50) NULL,
	[TipeKamar] [varchar](50) NULL,
	[Harga] [char](12) NULL,
	[LamaNginap] [varchar](50) NULL,
	[Total] [int] NULL,
	[Bayar] [int] NULL,
	[Kembalian] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_CEKOUT] PRIMARY KEY CLUSTERED 
(
	[KTP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FASILITASHOTEL]    Script Date: 5/27/2022 11:14:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FASILITASHOTEL](
	[IDFasilitas] [varchar](5) NOT NULL,
	[Nama] [varchar](50) NULL,
	[Lantai] [varchar](50) NULL,
	[Keterangan] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_FASILITASHOTEL] PRIMARY KEY CLUSTERED 
(
	[IDFasilitas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_KAMAR]    Script Date: 5/27/2022 11:14:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_KAMAR](
	[IDKamar] [varchar](5) NOT NULL,
	[Lantai] [varchar](50) NULL,
	[NoKamar] [varchar](50) NULL,
	[TipeKamar] [varchar](50) NULL,
	[Fasilitas] [varchar](100) NULL,
	[Harga] [char](12) NULL,
 CONSTRAINT [PK_TBL_KAMAR] PRIMARY KEY CLUSTERED 
(
	[IDKamar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_KARYAWAN]    Script Date: 5/27/2022 11:14:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_KARYAWAN](
	[IDKaryawan] [varchar](4) NOT NULL,
	[Nama] [varchar](50) NULL,
	[JenisKelamin] [varchar](50) NULL,
	[Alamat] [varchar](100) NULL,
	[NoTelp] [varchar](50) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](20) NULL,
	[Akses] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_KARYAWAN] PRIMARY KEY CLUSTERED 
(
	[IDKaryawan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TAMU]    Script Date: 5/27/2022 11:14:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TAMU](
	[KTP] [varchar](30) NOT NULL,
	[Nama] [varchar](50) NULL,
	[JenisKelamin] [varchar](50) NULL,
	[Alamat] [varchar](100) NULL,
	[NoTelp] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_TAMU] PRIMARY KEY CLUSTERED 
(
	[KTP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[TBL_CEKIN] ([IDKamar], [Lantai], [NoKamar], [TipeKamar], [Fasilitas], [Harga], [KTP], [Nama], [JenisKelamin], [Alamat], [NoTelp], [TglCekin]) VALUES (N'KM001', N'1', N'001', N'Superior', N'Tv,Kamar Mandi', N'500000      ', N'64002', N'Adji', N'Laki-Laki', N'Semarang, Jl.Roman No 54', N'085947473893', CAST(N'2022-05-12' AS Date))
INSERT [dbo].[TBL_CEKOUT] ([KTP], [Nama], [JenisKelamin], [Alamat], [NoTelp], [TglCekin], [TglCekout], [IDKamar], [Lantai], [NoKamar], [TipeKamar], [Harga], [LamaNginap], [Total], [Bayar], [Kembalian]) VALUES (N'64001', N'Alfonsus', N'Laki-Laki', N'Anggana, Jl.Masjid No 53', N'082174748382', CAST(N'2022-05-10 00:00:00.000' AS DateTime), CAST(N'2022-05-12 00:00:00.000' AS DateTime), N'KM001', N'1', N'001', N'Superior', N'500000      ', N'2', 1000000, 7000000, N'6000000')
INSERT [dbo].[TBL_FASILITASHOTEL] ([IDFasilitas], [Nama], [Lantai], [Keterangan]) VALUES (N'FH001', N'Kolam Renang', N'1, 4, 5', N'Berada Di Dekat Bar')
INSERT [dbo].[TBL_FASILITASHOTEL] ([IDFasilitas], [Nama], [Lantai], [Keterangan]) VALUES (N'FH002', N'Cafe', N'2, 4', N'Beradang Di Dekat Kolam Renang')
INSERT [dbo].[TBL_FASILITASHOTEL] ([IDFasilitas], [Nama], [Lantai], [Keterangan]) VALUES (N'FH003', N'Restoran', N'1, 3 , 6', N'Berada Di Ujung Jalan')
INSERT [dbo].[TBL_FASILITASHOTEL] ([IDFasilitas], [Nama], [Lantai], [Keterangan]) VALUES (N'FH004', N'Area bebas rokok', N'6', N'Berada Di Atap Hotel')
INSERT [dbo].[TBL_FASILITASHOTEL] ([IDFasilitas], [Nama], [Lantai], [Keterangan]) VALUES (N'FH005', N'Wi-Fi Internet gratis', N'1, 2, 3, 4, 5, 6', N'Tersedia Di Semua Ruangan')
INSERT [dbo].[TBL_KAMAR] ([IDKamar], [Lantai], [NoKamar], [TipeKamar], [Fasilitas], [Harga]) VALUES (N'KM001', N'1', N'001', N'Superior', N'Tv,Kamar Mandi', N'500000      ')
INSERT [dbo].[TBL_KAMAR] ([IDKamar], [Lantai], [NoKamar], [TipeKamar], [Fasilitas], [Harga]) VALUES (N'KM002', N'1', N'002', N'Superior', N'Tv, Kulkas', N'500000      ')
INSERT [dbo].[TBL_KAMAR] ([IDKamar], [Lantai], [NoKamar], [TipeKamar], [Fasilitas], [Harga]) VALUES (N'KM004', N'2', N'003', N'Deluxe', N'Ac,Kamar Mandi', N'800000      ')
INSERT [dbo].[TBL_KAMAR] ([IDKamar], [Lantai], [NoKamar], [TipeKamar], [Fasilitas], [Harga]) VALUES (N'KM005', N'2', N'005', N'Deluxe', N'Sarapan', N'800000      ')
INSERT [dbo].[TBL_KAMAR] ([IDKamar], [Lantai], [NoKamar], [TipeKamar], [Fasilitas], [Harga]) VALUES (N'KM006', N'4', N'004', N'deluxe', N'adada', N'242424242   ')
INSERT [dbo].[TBL_KARYAWAN] ([IDKaryawan], [Nama], [JenisKelamin], [Alamat], [NoTelp], [Username], [Password], [Akses]) VALUES (N'K001', N'admin', N'Laki-Laki', N'Samarinda, Jl Pahlawan No 43', N'088484783833', N'Admin', N'Admin', N'Admin')
INSERT [dbo].[TBL_KARYAWAN] ([IDKaryawan], [Nama], [JenisKelamin], [Alamat], [NoTelp], [Username], [Password], [Akses]) VALUES (N'K002', N'Putri Seli', N'Perempuan', N'Surabaya, Jl Hao No 66', N'085748493948', N'Putri', N'Putri', N'Resepsionis')
INSERT [dbo].[TBL_KARYAWAN] ([IDKaryawan], [Nama], [JenisKelamin], [Alamat], [NoTelp], [Username], [Password], [Akses]) VALUES (N'K003', N'Sultan', N'Laki-Laki', N'Bandung, Jl dako No 90', N'084874949499', N'Sultan', N'Sultan', N'Resepsionis')
INSERT [dbo].[TBL_KARYAWAN] ([IDKaryawan], [Nama], [JenisKelamin], [Alamat], [NoTelp], [Username], [Password], [Akses]) VALUES (N'K004', N'Renia', N'Perempuan', N'Bandung, Jl Melati No %6', N'084858494930', N'Renia', N'Renia', N'Resepsionis')
INSERT [dbo].[TBL_KARYAWAN] ([IDKaryawan], [Nama], [JenisKelamin], [Alamat], [NoTelp], [Username], [Password], [Akses]) VALUES (N'K005', N'Pupi', N'Perempuan', N'Malang, Jl Lole No 5', N'087478383993', N'Pupi', N'Pupi', N'Resepsionis')
INSERT [dbo].[TBL_TAMU] ([KTP], [Nama], [JenisKelamin], [Alamat], [NoTelp]) VALUES (N'64001', N'Alfonsus', N'Laki-Laki', N'Anggana, Jl.Masjid No 53', N'082174748382')
INSERT [dbo].[TBL_TAMU] ([KTP], [Nama], [JenisKelamin], [Alamat], [NoTelp]) VALUES (N'64002', N'Adji', N'Laki-Laki', N'Semarang, Jl.Roman No 54', N'085947473893')
INSERT [dbo].[TBL_TAMU] ([KTP], [Nama], [JenisKelamin], [Alamat], [NoTelp]) VALUES (N'64003', N'Fathur', N'Laki-Laki', N'Samarinda, Jl.Lati No 44', N'084646473883')
INSERT [dbo].[TBL_TAMU] ([KTP], [Nama], [JenisKelamin], [Alamat], [NoTelp]) VALUES (N'64004', N'Daud', N'Laki-Laki', N'Samarinda, Jl.Lami No 444', N'084778738383')
INSERT [dbo].[TBL_TAMU] ([KTP], [Nama], [JenisKelamin], [Alamat], [NoTelp]) VALUES (N'64005', N'Intan', N'Perempuan', N'Anggana, Jl.Masjid No 53', N'084773783333')
ALTER TABLE [dbo].[TBL_CEKIN]  WITH CHECK ADD  CONSTRAINT [FK_TBL_CEKIN_TBL_KAMAR] FOREIGN KEY([IDKamar])
REFERENCES [dbo].[TBL_KAMAR] ([IDKamar])
GO
ALTER TABLE [dbo].[TBL_CEKIN] CHECK CONSTRAINT [FK_TBL_CEKIN_TBL_KAMAR]
GO
ALTER TABLE [dbo].[TBL_CEKIN]  WITH CHECK ADD  CONSTRAINT [FK_TBL_CEKIN_TBL_TAMU] FOREIGN KEY([KTP])
REFERENCES [dbo].[TBL_TAMU] ([KTP])
GO
ALTER TABLE [dbo].[TBL_CEKIN] CHECK CONSTRAINT [FK_TBL_CEKIN_TBL_TAMU]
GO
USE [master]
GO
ALTER DATABASE [db_UKK_P2_Alfonsus] SET  READ_WRITE 
GO
