﻿Imports System.Data.SqlClient
Public Class FrmLogin
    Private Sub FrmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtpassword.Text = ""
        txtusername.Text = ""
        txtpassword.UseSystemPasswordChar = True
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        txtusername.Text = ""
        txtpassword.Text = ""
        CheckBox1.Checked = False
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            txtpassword.UseSystemPasswordChar = False
        Else
            txtpassword.UseSystemPasswordChar = True
        End If
        txtpassword.Focus()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Call Koneksi()
        Cmd = New SqlCommand("select * from TBL_KARYAWAN where Username='" & txtusername.Text & "' and Password= '" & txtpassword.Text & "'", Conn)
        Rd = Cmd.ExecuteReader
        Rd.Read()
        If Rd.HasRows Then
            If (Rd("Akses") = "Admin") Then
                FrmMenuUtama.Pengguna.Enabled = True
                FrmMenuUtama.Kamar.Enabled = True
                FrmMenuUtama.Hotel.Enabled = True
                FrmMenuUtama.Tamu.Enabled = True
                FrmMenuUtama.Checkin.Enabled = True
                FrmMenuUtama.Checkout.Enabled = True
                FrmMenuUtama.Laporan.Enabled = True
            ElseIf (Rd("Akses")) = "Resepsionis" Then
                FrmMenuUtama.Pengguna.Enabled = False
                FrmMenuUtama.Kamar.Enabled = False
                FrmMenuUtama.Hotel.Enabled = False
                FrmMenuUtama.Tamu.Enabled = True
                FrmMenuUtama.Checkin.Enabled = True
                FrmMenuUtama.Checkout.Enabled = True
                FrmMenuUtama.Laporan.Enabled = False
            End If
            FrmMenuUtama.Label8.Text = Rd!Nama
            CheckBox1.Checked = False
            txtusername.Text = ""
            txtpassword.Text = ""
            txtusername.Focus()
            Loading.Show()
            Me.Hide()
        Else
            MsgBox("Username Atau Password Salah!", MsgBoxStyle.Information, "Hotel Emon")
            CheckBox1.Checked = False
            txtusername.Text = ""
            txtpassword.Text = ""
            txtusername.Focus()
        End If
    End Sub

    Private Sub txtusername_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtusername.KeyPress
        If e.KeyChar = Chr(13) Then
            txtpassword.Focus()
        End If
    End Sub

    Private Sub txtpassword_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtpassword.KeyPress
        If e.KeyChar = Chr(13) Then
            Call Koneksi()
            Cmd = New SqlCommand("select * from TBL_KARYAWAN where Username='" & txtusername.Text & "' and Password= '" & txtpassword.Text & "'", Conn)
            Rd = Cmd.ExecuteReader
            Rd.Read()
            If Rd.HasRows Then
                If (Rd("Akses") = "Admin") Then
                    FrmMenuUtama.Pengguna.Enabled = True
                    FrmMenuUtama.Kamar.Enabled = True
                    FrmMenuUtama.Hotel.Enabled = True
                    FrmMenuUtama.Tamu.Enabled = True
                    FrmMenuUtama.Checkin.Enabled = True
                    FrmMenuUtama.Checkout.Enabled = True
                    FrmMenuUtama.Laporan.Enabled = True
                ElseIf (Rd("Akses")) = "Resepsionis" Then
                    FrmMenuUtama.Pengguna.Enabled = False
                    FrmMenuUtama.Kamar.Enabled = False
                    FrmMenuUtama.Hotel.Enabled = False
                    FrmMenuUtama.Tamu.Enabled = True
                    FrmMenuUtama.Checkin.Enabled = True
                    FrmMenuUtama.Checkout.Enabled = True
                    FrmMenuUtama.Laporan.Enabled = False
                End If
                FrmMenuUtama.Label8.Text = Rd!Nama
                CheckBox1.Checked = False
                txtusername.Text = ""
                txtpassword.Text = ""
                txtusername.Focus()
                Loading.Timer1.Stop()
                Loading.Show()
                Me.Hide()
            Else
                MsgBox("Username Atau Password Salah!", MsgBoxStyle.Information, "Hotel Emon")
                CheckBox1.Checked = False
                txtusername.Text = ""
                txtpassword.Text = ""
                txtusername.Focus()
            End If
        End If
    End Sub
End Class