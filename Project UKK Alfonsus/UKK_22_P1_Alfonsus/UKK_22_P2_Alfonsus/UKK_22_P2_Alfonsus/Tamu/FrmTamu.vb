﻿Imports System.Data.SqlClient
Public Class FrmTamu

    Sub KondisiAwal()
        txtid.Text = ""
        txtnama.Text = ""
        cbojenis.Text = ""
        txtalamat.Text = ""
        txttelp.Text = ""
        txtid.Enabled = False
        txtnama.Enabled = False
        cbojenis.Enabled = False
        txtalamat.Enabled = False
        txttelp.Enabled = False
        Button1.Text = "Tambah"
        Button2.Text = "Ubah"
        Button3.Text = "Hapus"
        Button4.Text = "Tutup"
        Button4.Enabled = True
        Button3.Enabled = True
        Button2.Enabled = True
        Button1.Enabled = True
        cbojenis.Items.Clear()
        cbojenis.Items.Add("Laki-Laki")
        cbojenis.Items.Add("Perempuan")
        Call Koneksi()
        Da = New SqlDataAdapter("select * from TBL_TAMU", Conn)
        Ds = New DataSet
        Da.Fill(Ds, "TBL_TAMU")
        DataGridView1.DataSource = (Ds.Tables("TBL_TAMU"))
        DataGridView1.Columns(0).HeaderText = "ID/No KTP"
        DataGridView1.Columns(1).HeaderText = "Nama"
        DataGridView1.Columns(2).HeaderText = "Jeni Kelamin"
        DataGridView1.Columns(3).HeaderText = "Alamat"
        DataGridView1.Columns(4).HeaderText = "Nomor Telepon"
        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
    End Sub

    Sub SiapIsi()
        txtid.Enabled = True
        txtnama.Enabled = True
        cbojenis.Enabled = True
        txtalamat.Enabled = True
        txttelp.Enabled = True
    End Sub

    Sub KunciHapus()
        txtid.Enabled = False
        txtnama.Enabled = False
        cbojenis.Enabled = False
        txtalamat.Enabled = False
        txttelp.Enabled = False
    End Sub

    Private Sub FrmTamu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call KondisiAwal()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Button1.Text = "Tambah" Then
            Button1.Text = "Simpan"
            Button2.Enabled = False
            Button3.Enabled = False
            Button4.Text = "Batal"
            Call SiapIsi()
        Else
            If txtid.Text = "" Or txtnama.Text = "" Or cbojenis.Text = "" Or txtalamat.Text = "" Or txttelp.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                Call Koneksi()
                Dim SimpanData As String = "insert into TBL_TAMU values('" & txtid.Text & "','" & txtnama.Text & "','" & cbojenis.Text & "','" & txtalamat.Text & "','" & txttelp.Text & "')"
                Cmd = New SqlCommand(SimpanData, Conn)
                Cmd.ExecuteNonQuery()
                MsgBox("Data Berhasil Di Simpan", MsgBoxStyle.Information, "Hotel Emon")
                Call KondisiAwal()
            End If
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Button2.Text = "Ubah" Then
            Button2.Text = "Simpan"
            Button1.Enabled = False
            Button3.Enabled = False
            Button4.Text = "Batal"
            Call SiapIsi()
        Else
            If txtid.Text = "" Or txtnama.Text = "" Or cbojenis.Text = "" Or txtalamat.Text = "" Or txttelp.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                Call Koneksi()
                Dim EditData As String = "update TBL_TAMU set Nama='" & txtnama.Text & "',JenisKelamin='" & cbojenis.Text & "',Alamat='" & txtalamat.Text & "',NoTelp='" & txttelp.Text & "' where KTP='" & txtid.Text & "'"
                Cmd = New SqlCommand(EditData, Conn)
                Cmd.ExecuteNonQuery()
                MsgBox("Data Berhasil Di Ubah", MsgBoxStyle.Information, "Hotel Emon")
                Call KondisiAwal()
            End If
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If Button3.Text = "Hapus" Then
            Button3.Text = "hapus"
            Button1.Enabled = False
            Button2.Enabled = False
            Button4.Text = "Batal"
            Call KunciHapus()
        Else
            If txtid.Text = "" Or txtnama.Text = "" Or cbojenis.Text = "" Or txtalamat.Text = "" Or txttelp.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                If MessageBox.Show("Apakah Anda Ingin Menghapus Data ?", "Hotel Emon", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                    Call Koneksi()
                    Dim HapusData As String = "delete TBL_TAMU where KTP='" & txtid.Text & "'"
                    Cmd = New SqlCommand(HapusData, Conn)
                    Cmd.ExecuteNonQuery()
                    MsgBox("Data Berhasil Di Hapus", MsgBoxStyle.Information, "Hotel Emon")
                    Call KondisiAwal()
                Else
                End If
            End If
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Button4.Text = "Tutup" Then
            Me.Close()
        Else
            Call KondisiAwal()
        End If
    End Sub

    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        If Button2.Text = "Simpan" Then
            Dim i As Integer
            i = DataGridView1.CurrentRow.Index

            On Error Resume Next
            txtid.Text = DataGridView1.Item(0, i).Value
            txtnama.Text = DataGridView1.Item(1, i).Value
            cbojenis.Text = DataGridView1.Item(2, i).Value
            txtalamat.Text = DataGridView1.Item(3, i).Value
            txttelp.Text = DataGridView1.Item(4, i).Value
        Else
            If Button3.Text = "hapus" Then
                Dim a As Integer
                a = DataGridView1.CurrentRow.Index

                On Error Resume Next
                txtid.Text = DataGridView1.Item(0, a).Value
                txtid.Text = DataGridView1.Item(0, a).Value
                txtnama.Text = DataGridView1.Item(1, a).Value
                cbojenis.Text = DataGridView1.Item(2, a).Value
                txtalamat.Text = DataGridView1.Item(3, a).Value
                txttelp.Text = DataGridView1.Item(4, a).Value
            Else
            End If
        End If
    End Sub

    Private Sub txtid_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtid.KeyPress
        If Not Char.IsNumber(e.KeyChar) And Not e.KeyChar = Chr(Keys.Delete) And Not e.KeyChar = Chr(Keys.Back) And Not e.KeyChar = Chr(Keys.Space) Then
            e.Handled = True
            MessageBox.Show("Silahkan isi Menggunakan Nomor")
        End If
        If txtid.Text.Length >= 14 Then
            If e.KeyChar <> ControlChars.Back Then
                e.Handled = True
                MessageBox.Show("Batas Input ID/ KTP Hanya 14 nomor")
            End If
        End If
    End Sub

    Private Sub txttelp_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txttelp.KeyPress
        If Not Char.IsNumber(e.KeyChar) And Not e.KeyChar = Chr(Keys.Delete) And Not e.KeyChar = Chr(Keys.Back) And Not e.KeyChar = Chr(Keys.Space) Then
            e.Handled = True
            MessageBox.Show("Silahkan isi Menggunakan Nomor")
        End If
        If txttelp.Text.Length >= 12 Then
            If e.KeyChar <> ControlChars.Back Then
                e.Handled = True
                MessageBox.Show("Batas Input Nomor Telefon Hanya 12 nomor")
            End If
        End If
    End Sub
End Class