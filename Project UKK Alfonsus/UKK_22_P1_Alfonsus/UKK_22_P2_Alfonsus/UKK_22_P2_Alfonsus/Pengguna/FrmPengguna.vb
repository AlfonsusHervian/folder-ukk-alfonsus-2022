﻿Imports System.Data.SqlClient
Public Class FrmPengguna

    Sub KondisiAwal()
        Call kodeotomatis()
        txtnama.Text = ""
        cbojenis.Text = ""
        txtalamat.Text = ""
        txttelp.Text = ""
        txtusername.Text = ""
        txtpassword.Text = ""
        txtnama.Enabled = False
        cbojenis.Enabled = False
        txtalamat.Enabled = False
        cbojenis.Enabled = False
        txttelp.Enabled = False
        txtusername.Enabled = False
        txtpassword.Enabled = False
        cboakses.Enabled = False
        CheckBox1.Enabled = False
        CheckBox1.Checked = False
        Button1.Text = "Tambah"
        Button2.Text = "Ubah"
        Button3.Text = "Hapus"
        Button4.Text = "Tutup"
        Button4.Enabled = True
        Button3.Enabled = True
        Button2.Enabled = True
        Button1.Enabled = True
        cbojenis.Items.Clear()
        cbojenis.Items.Add("Laki-Laki")
        cbojenis.Items.Add("Perempuan")
        cboakses.Items.Clear()
        cboakses.Items.Add("Admin")
        cboakses.Items.Add("Resepsionis")
        Call Koneksi()
        Da = New SqlDataAdapter("select IDKaryawan, Nama, JenisKelamin, Alamat, Notelp, Username, Akses from TBL_KARYAWAN", Conn)
        Ds = New DataSet
        Da.Fill(Ds, "TBL_KARYAWAN")
        DataGridView1.DataSource = (Ds.Tables("TBL_KARYAWAN"))
        DataGridView1.Columns(0).HeaderText = "ID Karyawan"
        DataGridView1.Columns(1).HeaderText = "Nama"
        DataGridView1.Columns(2).HeaderText = "Jenis Kelamin"
        DataGridView1.Columns(3).HeaderText = "Alamat"
        DataGridView1.Columns(4).HeaderText = "Nomor Telepon"
        DataGridView1.Columns(5).HeaderText = "Username"
        DataGridView1.Columns(6).HeaderText = "Akses"
        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub

    Sub SiapIsi()
        txtnama.Enabled = True
        cbojenis.Enabled = True
        txtalamat.Enabled = True
        cbojenis.Enabled = True
        txttelp.Enabled = True
        txtusername.Enabled = True
        txtpassword.Enabled = True
        cboakses.Enabled = True
        CheckBox1.Enabled = True
    End Sub

    Sub KunciHapus()
        txtnama.Enabled = True
        cbojenis.Enabled = False
        txtalamat.Enabled = False
        cbojenis.Enabled = False
        txttelp.Enabled = False
        txtusername.Enabled = False
        txtpassword.Enabled = False
        cboakses.Enabled = False
        CheckBox1.Enabled = False
    End Sub

    Sub kodeotomatis()
        Call Koneksi()
        cmd = New SqlCommand("select * from TBL_KARYAWAN where IDKARYAWAN in (select max(IDKARYAWAN) from TBL_KARYAWAN)", Conn)
        Dim UrutanKode As String
        Dim Hitung As Long
        Rd = cmd.ExecuteReader
        Rd.Read()
        If Not Rd.HasRows Then
            UrutanKode = "K" + "001"
        Else
            Hitung = Microsoft.VisualBasic.Right(Rd.GetString(0), 3) + 1
            UrutanKode = "K" + Microsoft.VisualBasic.Right("000" & Hitung, 3)
        End If
        txtid.Text = UrutanKode
    End Sub

    Private Sub FrmPengguna_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call KondisiAwal()
        Call kodeotomatis()
        txtid.Enabled = False
        txtpassword.UseSystemPasswordChar = True
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Button1.Text = "Tambah" Then
            Button1.Text = "Simpan"
            Button2.Enabled = False
            Button3.Enabled = False
            Button4.Text = "Batal"
            Call SiapIsi()
        Else
            If txtnama.Text = "" Or cbojenis.Text = "" Or txtalamat.Text = "" Or txttelp.Text = "" Or txtusername.Text = "" Or txtpassword.Text = "" Or cboakses.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                Call Koneksi()
                Dim SimpanData As String = "insert into TBL_KARYAWAN values('" & txtid.Text & "','" & txtnama.Text & "','" & cbojenis.Text & "','" & txtalamat.Text & "','" & txttelp.Text & "','" & txtusername.Text & "','" & txtpassword.Text & "','" & cboakses.Text & "')"
                Cmd = New SqlCommand(SimpanData, Conn)
                Cmd.ExecuteNonQuery()
                MsgBox("Data Berhasil Di Simpan", MsgBoxStyle.Information, "Hotel Emon")
                Call KondisiAwal()
            End If
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Button2.Text = "Ubah" Then
            Button2.Text = "Simpan"
            Button1.Enabled = False
            Button3.Enabled = False
            Button4.Text = "Batal"
            Call SiapIsi()
        Else
            If txtnama.Text = "" Or cbojenis.Text = "" Or txtalamat.Text = "" Or txttelp.Text = "" Or txtusername.Text = "" Or txtpassword.Text = "" Or cboakses.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                Call Koneksi()
                Dim EditData As String = "update TBL_KARYAWAN set Nama='" & txtnama.Text & "',JenisKelamin='" & cbojenis.Text & "',Alamat='" & txtalamat.Text & "',Notelp='" & txttelp.Text & "',Username='" & txtusername.Text & "',Password='" & txtpassword.Text & "',Akses='" & cboakses.Text & "' where IDKARYAWAN='" & txtid.Text & "'"
                Cmd = New SqlCommand(EditData, Conn)
                Cmd.ExecuteNonQuery()
                MsgBox("Data Berhasil Di Ubah", MsgBoxStyle.Information, "Hotel Emon")
                Call KondisiAwal()

            End If
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If Button3.Text = "Hapus" Then
            Button3.Text = "hapus"
            Button1.Enabled = False
            Button2.Enabled = False
            Button4.Text = "Batal"
            Call KunciHapus()
        Else
            If txtnama.Text = "" Or cbojenis.Text = "" Or txtalamat.Text = "" Or txttelp.Text = "" Or txtusername.Text = "" Or txtpassword.Text = "" Or cboakses.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                If MessageBox.Show("Apakah Anda Ingin Menghapus Data ?", "Hotel Emon", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                    Call Koneksi()
                    Dim HapusData As String = "delete tbl_karyawan where idkaryawan='" & txtid.Text & "'"
                    Cmd = New SqlCommand(HapusData, Conn)
                    Cmd.ExecuteNonQuery()
                    MsgBox("Data Berhasil Di Hapus", MsgBoxStyle.Information, "Hotel Emon")
                    Call KondisiAwal()
                Else
                End If
            End If
        End If
    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles Button4.Click
        If Button4.Text = "Tutup" Then
            Me.Close()

        Else
            Call KondisiAwal()

        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            txtpassword.UseSystemPasswordChar = False
        Else
            txtpassword.UseSystemPasswordChar = True
        End If
        txtpassword.Focus()
    End Sub

    Private Sub txtnama_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtnama.KeyPress
        If Button2.Text = "Simpan" Or Button3.Text = "hapus" Then
            If e.KeyChar = Chr(13) Then
                Call Koneksi()
                Cmd = New SqlCommand("Select * from tbl_karyawan where Nama='" & txtnama.Text & "'", Conn)
                Rd = Cmd.ExecuteReader
                Rd.Read()
                If Rd.HasRows Then
                    txtid.Text = Rd.Item("IDKaryawan")
                    cbojenis.Text = Rd.Item("JenisKelamin")
                    txtalamat.Text = Rd.Item("Alamat")
                    txttelp.Text = Rd.Item("NoTelp")
                    txtusername.Text = Rd.Item("Username")
                    txtpassword.Text = Rd.Item("Password")
                    cboakses.Text = Rd.Item("Akses")
                Else
                    MsgBox("Data Tidak Ditemukan")
                End If
            End If
        End If
    End Sub

    Private Sub txttelp_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txttelp.KeyPress
        If Not Char.IsNumber(e.KeyChar) And Not e.KeyChar = Chr(Keys.Delete) And Not e.KeyChar = Chr(Keys.Back) And Not e.KeyChar = Chr(Keys.Space) Then
            e.Handled = True
            MessageBox.Show("Silahkan isi Menggunakan Nomor")
        End If
        If txttelp.Text.Length >= 12 Then
            If e.KeyChar <> ControlChars.Back Then
                e.Handled = True
                MessageBox.Show("Batas Input Nomor Telefon Hanya 12 nomor")
            End If
        End If
    End Sub
End Class