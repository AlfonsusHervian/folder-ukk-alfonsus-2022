﻿Imports System.Data.SqlClient
Public Class TampilDataCekIn
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand

    Sub TampilAwal()
        txtcari.Text = ""
        Call Koneksi()
        Da = New SqlDataAdapter("select Nama, JenisKelamin, Alamat, Notelp, TipeKamar, NoKamar, Lantai, tglcekin from TBL_CEKIN", Conn)
        Ds = New DataSet
        Da.Fill(Ds, "TBL_CEKIN")
        DataGridView1.DataSource = (Ds.Tables("TBL_CEKIN"))
        DataGridView1.Columns(0).HeaderText = "Nama"
        DataGridView1.Columns(1).HeaderText = "Jenis Kelamin"
        DataGridView1.Columns(2).HeaderText = "Alamat"
        DataGridView1.Columns(3).HeaderText = "Nomor Telepon"
        DataGridView1.Columns(4).HeaderText = "Tipe Kamar"
        DataGridView1.Columns(5).HeaderText = "Nomor Kamar"
        DataGridView1.Columns(6).HeaderText = "Lantai"
        DataGridView1.Columns(7).HeaderText = "Tanggal Check In"
        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
    End Sub
    Private Sub TampilDataCekIn_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_UKK_P2_Alfonsus;Integrated Security=True"
        Call TampilAwal()
        Call Koneksi()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        FrmCekIn.ShowDialog()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub txtcari_TextChanged(sender As Object, e As EventArgs) Handles txtcari.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT Nama, JenisKelamin, Alamat, NoTelp, TipeKamar, NoKamar, Lantai, tglcekin from TBL_CEKIN WHERE Nama LIKE '%" & txtcari.Text & "%' OR JenisKelamin LIKE '%" & txtcari.Text & "%' OR Alamat LIKE '%" & txtcari.Text & "%'OR NoTelp LIKE '%" & txtcari.Text & "%'OR TipeKamar LIKE '%" & txtcari.Text & "%'OR NoKamar LIKE '%" & txtcari.Text & "%'OR tglcekin LIKE '%" & txtcari.Text & "%'"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()
    End Sub
End Class