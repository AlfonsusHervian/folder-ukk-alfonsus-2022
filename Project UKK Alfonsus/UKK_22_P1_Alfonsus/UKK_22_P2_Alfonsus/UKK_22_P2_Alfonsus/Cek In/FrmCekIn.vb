﻿Imports System.Data.SqlClient
Public Class FrmCekIn

    Sub KondisiAwal()
        Call MunculIDKTP()
        Call MunculIDKAMAR()
        cboktp.Text = ""
        cbokamar.Text = ""
        txtlantai.Text = ""
        txtnokamar.Text = ""
        txttipekamar.Text = ""
        txtfasilitas.Text = ""
        txtharga.Text = ""
        txtnama.Text = ""
        txtjeniskelamin.Text = ""
        txtalamat.Text = ""
        txtnotelp.Text = ""

        cbokamar.Enabled = False
        txtlantai.Enabled = False
        txtnokamar.Enabled = False
        txttipekamar.Enabled = False
        txtfasilitas.Enabled = False
        txtharga.Enabled = False
        cboktp.Enabled = False
        txtnama.Enabled = False
        txtjeniskelamin.Enabled = False
        txtalamat.Enabled = False
        txtnotelp.Enabled = False

        Button1.Text = "Tambah"
        Button4.Text = "Tutup"
        Button4.Enabled = True
        Button1.Enabled = True
    End Sub

    Sub SiapIsi()
        cbokamar.Enabled = True
        cboktp.Enabled = True
    End Sub

    Sub MunculIDKAMAR()
        Call Koneksi()
        cbokamar.Items.Clear()
        Cmd = New SqlCommand("select * from TBL_KAMAR", Conn)
        Rd = Cmd.ExecuteReader
        Do While Rd.Read
            cbokamar.Items.Add(Rd.Item("IDKamar"))
        Loop
    End Sub

    Sub MunculIDKTP()
        Call Koneksi()
        cbokamar.Items.Clear()
        Cmd = New SqlCommand("select * from TBL_TAMU", Conn)
        Rd = Cmd.ExecuteReader
        Do While Rd.Read
            cboktp.Items.Add(Rd.Item("KTP"))
        Loop
    End Sub

    Private Sub FrmCekIn_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call KondisiAwal()
        TglCekin.Text = Today
    End Sub

    Private Sub cbokamar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbokamar.SelectedIndexChanged
        Call Koneksi()
        Cmd = New SqlCommand("select * from TBL_KAMAR where IDKamar ='" & cbokamar.Text & "'", Conn)
        Rd = Cmd.ExecuteReader
        Rd.Read()
        If Rd.HasRows Then
            txtlantai.Text = Rd!Lantai
            txtnokamar.Text = Rd!NoKamar
            txttipekamar.Text = Rd!TipeKamar
            txtfasilitas.Text = Rd!Fasilitas
            txtharga.Text = Rd!Harga
        End If
    End Sub

    Private Sub cboktp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboktp.SelectedIndexChanged
        Call Koneksi()
        Cmd = New SqlCommand("select * from TBL_TAMU where KTP ='" & cboktp.Text & "'", Conn)
        Rd = Cmd.ExecuteReader
        Rd.Read()
        If Rd.HasRows Then
            txtnama.Text = Rd!Nama
            txtjeniskelamin.Text = Rd!JenisKelamin
            txtalamat.Text = Rd!Alamat
            txtnotelp.Text = Rd!NoTelp
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Button1.Text = "Tambah" Then
            Button1.Text = "Simpan"
            Button4.Text = "Batal"
            Call SiapIsi()
        Else
            If cbokamar.Text = "" Or cboktp.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                    Call Koneksi()
                    Cmd = New SqlCommand("select * from TBL_CEKIN where KTP='" & cboktp.Text & "'", Conn)
                    Rd = Cmd.ExecuteReader
                    Rd.Read()
                    If Rd.HasRows = True Then
                        MsgBox("Tamu Sudah Tercekin", MsgBoxStyle.Information, "Hotel Emon")
                Else
                    Call Koneksi()
                    Cmd = New SqlCommand("select * from TBL_CEKIN where IDKAMAR='" & cbokamar.Text & "'", Conn)
                    Rd = Cmd.ExecuteReader
                    Rd.Read()
                    If Rd.HasRows = True Then
                        MsgBox("Kamar Sudah Terisi", MsgBoxStyle.Information, "Hotel Emon")
                    Else
                        Call Koneksi()
                        Cmd = New SqlCommand("select * from TBL_CEKIN where IDKAMAR='" & cbokamar.Text & "' and KTP='" & cboktp.Text & "'", Conn)
                        Rd = Cmd.ExecuteReader
                        Rd.Read()
                        Call Koneksi()
                        Dim SimpanData As String = "insert into TBL_CEKIN values('" & cbokamar.Text & "','" & txtlantai.Text & "','" & txtnokamar.Text & "','" & txttipekamar.Text & "','" & txtfasilitas.Text & "','" & txtharga.Text & "','" & cboktp.Text & "','" & txtnama.Text & "','" & txtjeniskelamin.Text & "','" & txtalamat.Text & "','" & txtnotelp.Text & "','" & TglCekin.Value.ToString("dd MMM yyy") & "')"
                        Cmd = New SqlCommand(SimpanData, Conn)
                        Cmd.ExecuteNonQuery()
                        MsgBox("Data Berhasil Di Simpan", MsgBoxStyle.Information, "Hotel Emon")
                        cbokamar.Items.Clear()
                        cboktp.Items.Clear()
                        Call KondisiAwal()
                        Call TampilDataCekIn.TampilAwal()
                        cboktp.Items.Clear()
                        cbokamar.Items.Clear()
                        Me.Close()
                    End If
                    End If
                End If
            End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Button4.Text = "Tutup" Then
            cboktp.Items.Clear()
            cbokamar.Items.Clear()
            Me.Close()
        Else
            cboktp.Items.Clear()
            cbokamar.Items.Clear()
            Call KondisiAwal()
        End If
    End Sub
End Class