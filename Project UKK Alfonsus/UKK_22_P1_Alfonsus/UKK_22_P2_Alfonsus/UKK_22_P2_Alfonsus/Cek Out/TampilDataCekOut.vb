﻿Imports System.Data.SqlClient
Public Class TampilDataCekOut
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand

    Sub TampilAwal()
        txtcari.Text = ""
        Call Koneksi()
        Da = New SqlDataAdapter("select Nama, Alamat, NoKamar, TipeKamar, TglCekin, TglCekout, Harga from TBL_CEKOUT", Conn)
        Ds = New DataSet
        Da.Fill(Ds, "TBL_CEKOUT")
        DataGridView1.DataSource = (Ds.Tables("TBL_CEKOUT"))
        DataGridView1.Columns(0).HeaderText = "Nama"
        DataGridView1.Columns(1).HeaderText = "Alamat"
        DataGridView1.Columns(2).HeaderText = "Nomor Kamar"
        DataGridView1.Columns(3).HeaderText = "Tipe Kamar"
        DataGridView1.Columns(4).HeaderText = "Tanggal Check In"
        DataGridView1.Columns(5).HeaderText = "Tanggal Check Out"
        DataGridView1.Columns(6).HeaderText = "Total Harga"
        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        FrmCekOut.ShowDialog()
    End Sub

    Private Sub TampilDataCekOut_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_UKK_P2_Alfonsus;Integrated Security=True"
        Call TampilAwal()
        Call Koneksi()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtcari.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT Nama, Alamat, NoKamar, TipeKamar, TglCekin, TglCekout, Bayar from TBL_CEKOUT WHERE Nama LIKE '%" & txtcari.Text & "%' OR ALamat LIKE '%" & txtcari.Text & "%' OR NoKamar LIKE '%" & txtcari.Text & "%'OR TipeKamar LIKE '%" & txtcari.Text & "%'OR TglCekin LIKE '%" & txtcari.Text & "%'OR TglCekout LIKE '%" & txtcari.Text & "%'OR Bayar LIKE '%" & txtcari.Text & "%'"
        Dim rd As SqlDataReader = Cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()
    End Sub
End Class