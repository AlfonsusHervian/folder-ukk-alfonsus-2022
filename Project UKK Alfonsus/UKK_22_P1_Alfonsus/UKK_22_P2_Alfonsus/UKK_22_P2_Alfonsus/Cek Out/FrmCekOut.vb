﻿Imports System.Data.SqlClient
Imports System.Drawing.Printing
Public Class FrmCekOut
    Dim WithEvents PD As New PrintDocument
    Dim PPD As New PrintPreviewDialog

    Sub KondisiAwal()
        Button1.Focus()
        Call MunculIDKAMAR()
        cboktp.Text = ""
        txtnama.Text = ""
        txtjeniskelamin.Text = ""
        txtalamat.Text = ""
        txtnotelp.Text = ""
        tglcekin.Text = ""
        tglcekout.Text = ""
        txtIdkamar.Text = ""
        txtlantai.Text = ""
        txtNoKamar.Text = ""
        txttipekamar.Text = ""
        txtharga.Text = ""
        txtlamanginap.Text = ""
        txttotal.Text = ""
        txtbayar.Text = ""
        txtkembali.Text = ""

        cboktp.Enabled = False
        txtnama.Visible = False
        txtjeniskelamin.Visible = False
        txtalamat.Visible = False
        txtnotelp.Visible = False
        tglcekin.Visible = False
        tglcekout.Visible = False
        txtIdkamar.Visible = False
        txtlantai.Visible = False
        txtNoKamar.Visible = False
        txttipekamar.Visible = False
        txtharga.Visible = False
        txtlamanginap.Visible = False
        Label14.Visible = False
        txttotal.Enabled = False
        txtbayar.Enabled = False
        txtkembali.Enabled = False
        txtlamanginap.Enabled = False

        Button1.Text = "Tambah"
        Button4.Text = "Tutup"
        Button4.Enabled = True
        Button1.Enabled = True
    End Sub

    Sub TampilDataCekin()
        txtnama.Visible = True
        txtjeniskelamin.Visible = True
        txtalamat.Visible = True
        txtnotelp.Visible = True
        tglcekin.Visible = True
        tglcekout.Visible = True
        txtIdkamar.Visible = True
        txtlantai.Visible = True
        txtNoKamar.Visible = True
        txttipekamar.Visible = True
        txtharga.Visible = True
        txtlamanginap.Visible = True
        Label14.Visible = True

        txtnama.Enabled = False
        txtjeniskelamin.Enabled = False
        txtalamat.Enabled = False
        txtnotelp.Enabled = False
        tglcekin.Enabled = False
        tglcekout.Enabled = False
        txtIdkamar.Enabled = False
        txtlantai.Enabled = False
        txtNoKamar.Enabled = False
        txttipekamar.Enabled = False
        txtharga.Enabled = False
        txtlamanginap.Enabled = False

    End Sub

    Sub SiapIsi()
        cboktp.Enabled = True
        txtbayar.Enabled = True
    End Sub

    Sub tglcekouut()
        tglcekout.Text = Today
    End Sub

    Sub lamaNginap()
        If tglcekin.Value < tglcekout.Value Then
            txtlamanginap.Text = DateDiff(DateInterval.Day, CDate(tglcekin.Text), CDate(tglcekout.Text))
        End If
    End Sub

    Sub MunculIDKAMAR()
        Call Koneksi()
        cboktp.Items.Clear()
        Cmd = New SqlCommand("select * from TBL_CEKIN", Conn)
        Rd = Cmd.ExecuteReader
        Do While Rd.Read
            cboktp.Items.Add(Rd.Item("KTP"))
        Loop
    End Sub

    Private Sub FrmCekOut_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Button1.Focus()
        Call KondisiAwal()
    End Sub

    Private Sub cboktp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboktp.SelectedIndexChanged
        Call Koneksi()
        Cmd = New SqlCommand("select * from TBL_CEKIN where KTP ='" & cboktp.Text & "'", Conn)
        Rd = Cmd.ExecuteReader
        Rd.Read()
        If Rd.HasRows Then
            Call tglcekouut()
            Call TampilDataCekin()
            txtnama.Text = Rd!Nama
            txtjeniskelamin.Text = Rd!JenisKelamin
            txtalamat.Text = Rd!Alamat
            txtnotelp.Text = Rd!NoTelp
            tglcekin.Text = Rd!TglCekin
            txtIdkamar.Text = Rd!IDKamar
            txtlantai.Text = Rd!Lantai
            txtNoKamar.Text = Rd!NoKamar
            txttipekamar.Text = Rd!TipeKamar
            txtharga.Text = Rd!Harga
            Call lamaNginap()
        End If
    End Sub

    Private Sub txtbayar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtbayar.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
        If e.KeyChar = Chr(13) Then
            If Val(txttotal.Text) >= Val(txtbayar.Text) Then
                MsgBox("Pembayaran Tidak Cukup ! ", MsgBoxStyle.Information, "Hotel Emon")
            Else
                Me.Button1.Focus()

                Dim d, c, f As New Long
                d = Val(txttotal.Text)
                c = Val(txtbayar.Text)

                f = c - d
                txtkembali.Text = f
            End If
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Button1.Text = "Tambah" Then
            Button1.Text = "Check Out"
            Button4.Text = "Batal"
            Call SiapIsi()
        Else
            If cboktp.Text = "" Or txtbayar.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                If Val(txttotal.Text) >= Val(txtbayar.Text) Then
                    MsgBox("Pembayaran Tidak Cukup ! ", MsgBoxStyle.Information, "Hotel Emon")
                Else
                    Call Koneksi()
                    Dim SimpanData As String = "insert into TBL_CEKOUT values('" & cboktp.Text & "','" & txtnama.Text & "','" & txtjeniskelamin.Text & "','" & txtalamat.Text & "','" & txtnotelp.Text & "','" & tglcekin.Value.ToString("dd MMM yyy") & "','" & tglcekout.Value.ToString("dd MMM yyy") & "','" & txtIdkamar.Text & "','" & txtlantai.Text & "','" & txtNoKamar.Text & "','" & txttipekamar.Text & "','" & txtharga.Text & "','" & txtlamanginap.Text & "','" & txttotal.Text & "','" & txtbayar.Text & "','" & txtkembali.Text & "')"
                    Cmd = New SqlCommand(SimpanData, Conn)
                    Cmd.ExecuteNonQuery()
                    Call Koneksi()
                    Dim HapusData As String = "delete TBL_CEKIN where KTP='" & cboktp.Text & "'"
                    Cmd = New SqlCommand(HapusData, Conn)
                    Cmd.ExecuteNonQuery()
                    MsgBox("Data Berhasil Di Simpan", MsgBoxStyle.Information, "Hotel Emon")
                    PPD.Document = PD
                    PPD.ShowDialog()
                    Me.Close()
                    cboktp.Items.Clear()
                    cboktp.Items.Clear()
                    Call KondisiAwal()
                    Call TampilDataCekOut.TampilAwal()
                End If
            End If
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Button4.Text = "Tutup" Then
            cboktp.Items.Clear()
            cboktp.Items.Clear()
            Me.Close()
        Else
            cboktp.Items.Clear()
            Call KondisiAwal()
        End If
    End Sub

    Private Sub txtlamanginap_TextChanged(sender As Object, e As EventArgs) Handles txtlamanginap.TextChanged
        Me.txtbayar.Focus()

        Dim a, b, c As New Long
        a = Val(txtlamanginap.Text)
        b = Val(txtharga.Text)

        c = a * b
        txttotal.Text = c
    End Sub


    Private Sub PD_BeginPrint(sender As Object, e As PrintEventArgs) Handles PD.BeginPrint
        Dim pagesetup As New PageSettings
        pagesetup.PaperSize = New PaperSize("Custom", 250, 340)
        PD.DefaultPageSettings = pagesetup
    End Sub

    Private Sub PD_PrintDocument1_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PD.PrintPage
        Dim f10 As New Font("Times New Roman", 10, FontStyle.Regular)
        Dim f10b As New Font("Times New Roman", 10, FontStyle.Bold)
        Dim f14 As New Font("Times New Roman", 12, FontStyle.Bold)
        Dim f15 As New Font("Times New Roman", 7, FontStyle.Regular)

        Dim leftmargin As Integer = PD.DefaultPageSettings.Margins.Left
        Dim centermargin As Integer = PD.DefaultPageSettings.PaperSize.Width / 2
        Dim rightmargin As Integer = PD.DefaultPageSettings.PaperSize.Width

        Dim kanan As New StringFormat
        Dim tengah As New StringFormat
        kanan.Alignment = StringAlignment.Far
        tengah.Alignment = StringAlignment.Center

        Dim garis As String
        garis = "---------------------------------------------------"

        e.Graphics.DrawString("Hotel Emon", f14, Brushes.Black, centermargin, 5, tengah)
        e.Graphics.DrawString("Jl. Pahlawan No. 2A, Kec. Samarinda Ulu, Kota Samarinda", f15, Brushes.Black, centermargin, 25, tengah)
        e.Graphics.DrawString("Kalimantan Timur 75123", f15, Brushes.Black, centermargin, 37, tengah)

        e.Graphics.DrawString("Kasir", f10, Brushes.Black, 0, 60)
        e.Graphics.DrawString(":", f10, Brushes.Black, 85, 60)
        e.Graphics.DrawString(FrmMenuUtama.Label8.Text, f10, Brushes.Black, 95, 60)

        e.Graphics.DrawString("Tanggal", f10, Brushes.Black, 0, 80)
        e.Graphics.DrawString(":", f10, Brushes.Black, 85, 80)
        e.Graphics.DrawString(FrmMenuUtama.Label4.Text, f10, Brushes.Black, 95, 80)

        e.Graphics.DrawString(garis, f10, Brushes.Black, 0, 95)

        Dim tinggi1, tinggi2 As New Integer
        Dim a As Double
        a = txttotal.Text
        tinggi1 = 250
        tinggi2 = 270

        e.Graphics.DrawString("Nama", f10, Brushes.Black, 0, 110)
        e.Graphics.DrawString(":", f10, Brushes.Black, 85, 110)
        e.Graphics.DrawString(txtnama.Text, f10, Brushes.Black, 95, 110)

        e.Graphics.DrawString("Jenis Kelamin", f10, Brushes.Black, 0, 130)
        e.Graphics.DrawString(":", f10, Brushes.Black, 85, 130)
        e.Graphics.DrawString(txtjeniskelamin.Text, f10, Brushes.Black, 95, 130)

        e.Graphics.DrawString("Nomor Kamar", f10, Brushes.Black, 0, 150)
        e.Graphics.DrawString(":", f10, Brushes.Black, 85, 150)
        e.Graphics.DrawString(txtNoKamar.Text, f10, Brushes.Black, 95, 150)

        e.Graphics.DrawString("Tipe Kamar", f10, Brushes.Black, 0, 170)
        e.Graphics.DrawString(":", f10, Brushes.Black, 85, 170)
        e.Graphics.DrawString(txttipekamar.Text, f10, Brushes.Black, 95, 170)

        e.Graphics.DrawString("Lama Nginap", f10, Brushes.Black, 0, 190)
        e.Graphics.DrawString(":", f10, Brushes.Black, 85, 190)
        e.Graphics.DrawString(txtlamanginap.Text + "  Hari", f10, Brushes.Black, 95, 190)

        e.Graphics.DrawString(garis, f10, Brushes.Black, 0, 210)
        e.Graphics.DrawString("Total Harga", f10, Brushes.Black, 0, 230)
        e.Graphics.DrawString(":", f10, Brushes.Black, 85, 230)
        txttotal.Text = Format(a, "##0")
        a = txttotal.Text
        txttotal.Text = Format(a, "Rp, ##,##0")
        e.Graphics.DrawString(txttotal.Text, f10, Brushes.Black, 95, 230)

        e.Graphics.DrawString("~Terimah Kasih Telah Berkunjung~", f10, Brushes.Black, centermargin, 35 + tinggi1, tengah)
        e.Graphics.DrawString("~Di Hotel Kami~", f10, Brushes.Black, centermargin, 35 + tinggi2, tengah)

    End Sub
End Class