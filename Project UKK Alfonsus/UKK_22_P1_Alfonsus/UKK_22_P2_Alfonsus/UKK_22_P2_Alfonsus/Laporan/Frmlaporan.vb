﻿Imports CrystalDecisions.CrystalReports.Engine
Imports Microsoft.VisualBasic
Public Class Frmlaporan


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim Lap As New ReportDocument
        Lap.Load("Laporan.rpt")
        If rbPerTanggal.Checked = True Then
            FrmTampilLap.CRV.SelectionFormula = "{TBL_CEKOUT.TglCekout}=date(" & Year(dtPerTanggal.Value) & "," & Month(dtPerTanggal.Value) & "," & Day(dtPerTanggal.Value) & ")"
        ElseIf rbPerPeriode.Checked = True Then
            FrmTampilLap.CRV.SelectionFormula = "{TBL_CEKOUT.TglCekout}>=date(" & Year(dtPerPeriode1.Value) & "," & Month(dtPerPeriode1.Value) & "," & Day(dtPerPeriode1.Value) & ") AND {TBL_CEKOUT.TglCekout}<=date(" & Year(dtPerPeriode2.Value) & "," & Month(dtPerPeriode2.Value) & "," & Day(dtPerPeriode2.Value) & ")"
        ElseIf rbPerBulan.Checked = True Then
            FrmTampilLap.CRV.SelectionFormula = "MONTH({TBL_CEKOUT.TglCekout})=" & CboBulan.SelectedIndex & " AND YEAR({TBL_CEKOUT.TglCekout})=" & Format(dtBulan.Value, "yyyy") & ""
        ElseIf rbPerTahun.Checked = True Then
            FrmTampilLap.CRV.SelectionFormula = "YEAR({TBL_CEKOUT.TglCekout})=" & Format(dtTahun.Value, "yyyy") & ""
        End If
        FrmTampilLap.CRV.ReportSource = Lap
        FrmTampilLap.CRV.RefreshReport()
        FrmTampilLap.ShowDialog()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class