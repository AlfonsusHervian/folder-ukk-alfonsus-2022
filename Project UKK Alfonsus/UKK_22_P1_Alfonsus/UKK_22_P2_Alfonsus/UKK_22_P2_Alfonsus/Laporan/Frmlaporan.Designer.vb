﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frmlaporan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frmlaporan))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtBulan = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtTahun = New System.Windows.Forms.DateTimePicker()
        Me.rbPerBulan = New System.Windows.Forms.RadioButton()
        Me.CboBulan = New System.Windows.Forms.ComboBox()
        Me.dtPerPeriode2 = New System.Windows.Forms.DateTimePicker()
        Me.dtPerPeriode1 = New System.Windows.Forms.DateTimePicker()
        Me.dtPerTanggal = New System.Windows.Forms.DateTimePicker()
        Me.rbPerTahun = New System.Windows.Forms.RadioButton()
        Me.rbPerPeriode = New System.Windows.Forms.RadioButton()
        Me.rbPerTanggal = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.DarkCyan
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.dtBulan)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.dtTahun)
        Me.Panel1.Controls.Add(Me.rbPerBulan)
        Me.Panel1.Controls.Add(Me.CboBulan)
        Me.Panel1.Controls.Add(Me.dtPerPeriode2)
        Me.Panel1.Controls.Add(Me.dtPerPeriode1)
        Me.Panel1.Controls.Add(Me.dtPerTanggal)
        Me.Panel1.Controls.Add(Me.rbPerTahun)
        Me.Panel1.Controls.Add(Me.rbPerPeriode)
        Me.Panel1.Controls.Add(Me.rbPerTanggal)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(0, -2)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(677, 492)
        Me.Panel1.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Franklin Gothic Demi", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(384, 219)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 18)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Tahun"
        '
        'dtBulan
        '
        Me.dtBulan.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtBulan.CustomFormat = "yyyy"
        Me.dtBulan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtBulan.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtBulan.Location = New System.Drawing.Point(436, 219)
        Me.dtBulan.Margin = New System.Windows.Forms.Padding(2)
        Me.dtBulan.Name = "dtBulan"
        Me.dtBulan.ShowUpDown = True
        Me.dtBulan.Size = New System.Drawing.Size(151, 21)
        Me.dtBulan.TabIndex = 16
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Demi", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(387, 186)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 18)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "s/d"
        '
        'dtTahun
        '
        Me.dtTahun.CustomFormat = "yyyy"
        Me.dtTahun.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTahun.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtTahun.Location = New System.Drawing.Point(213, 251)
        Me.dtTahun.Margin = New System.Windows.Forms.Padding(2)
        Me.dtTahun.Name = "dtTahun"
        Me.dtTahun.ShowUpDown = True
        Me.dtTahun.Size = New System.Drawing.Size(151, 21)
        Me.dtTahun.TabIndex = 15
        '
        'rbPerBulan
        '
        Me.rbPerBulan.AutoSize = True
        Me.rbPerBulan.Font = New System.Drawing.Font("Franklin Gothic Demi", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPerBulan.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.rbPerBulan.Location = New System.Drawing.Point(112, 216)
        Me.rbPerBulan.Margin = New System.Windows.Forms.Padding(2)
        Me.rbPerBulan.Name = "rbPerBulan"
        Me.rbPerBulan.Size = New System.Drawing.Size(87, 22)
        Me.rbPerBulan.TabIndex = 14
        Me.rbPerBulan.TabStop = True
        Me.rbPerBulan.Text = "Per Bulan"
        Me.rbPerBulan.UseVisualStyleBackColor = True
        '
        'CboBulan
        '
        Me.CboBulan.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboBulan.FormattingEnabled = True
        Me.CboBulan.Items.AddRange(New Object() {"--Pilih Bulan--", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"})
        Me.CboBulan.Location = New System.Drawing.Point(213, 218)
        Me.CboBulan.Margin = New System.Windows.Forms.Padding(2)
        Me.CboBulan.Name = "CboBulan"
        Me.CboBulan.Size = New System.Drawing.Size(151, 21)
        Me.CboBulan.TabIndex = 13
        Me.CboBulan.Text = "--Pilih Bulan--"
        '
        'dtPerPeriode2
        '
        Me.dtPerPeriode2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtPerPeriode2.CustomFormat = "dd-MM-yyyy"
        Me.dtPerPeriode2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtPerPeriode2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtPerPeriode2.Location = New System.Drawing.Point(436, 184)
        Me.dtPerPeriode2.Margin = New System.Windows.Forms.Padding(2)
        Me.dtPerPeriode2.Name = "dtPerPeriode2"
        Me.dtPerPeriode2.Size = New System.Drawing.Size(151, 21)
        Me.dtPerPeriode2.TabIndex = 12
        '
        'dtPerPeriode1
        '
        Me.dtPerPeriode1.CustomFormat = "dd-MM-yyyy"
        Me.dtPerPeriode1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtPerPeriode1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtPerPeriode1.Location = New System.Drawing.Point(213, 184)
        Me.dtPerPeriode1.Margin = New System.Windows.Forms.Padding(2)
        Me.dtPerPeriode1.Name = "dtPerPeriode1"
        Me.dtPerPeriode1.Size = New System.Drawing.Size(151, 21)
        Me.dtPerPeriode1.TabIndex = 11
        '
        'dtPerTanggal
        '
        Me.dtPerTanggal.CustomFormat = "dd-MM-yyyy"
        Me.dtPerTanggal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtPerTanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtPerTanggal.Location = New System.Drawing.Point(213, 149)
        Me.dtPerTanggal.Margin = New System.Windows.Forms.Padding(2)
        Me.dtPerTanggal.Name = "dtPerTanggal"
        Me.dtPerTanggal.Size = New System.Drawing.Size(151, 21)
        Me.dtPerTanggal.TabIndex = 10
        '
        'rbPerTahun
        '
        Me.rbPerTahun.AutoSize = True
        Me.rbPerTahun.Font = New System.Drawing.Font("Franklin Gothic Demi", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPerTahun.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.rbPerTahun.Location = New System.Drawing.Point(112, 251)
        Me.rbPerTahun.Margin = New System.Windows.Forms.Padding(2)
        Me.rbPerTahun.Name = "rbPerTahun"
        Me.rbPerTahun.Size = New System.Drawing.Size(89, 22)
        Me.rbPerTahun.TabIndex = 9
        Me.rbPerTahun.TabStop = True
        Me.rbPerTahun.Text = "Per Tahun"
        Me.rbPerTahun.UseVisualStyleBackColor = True
        '
        'rbPerPeriode
        '
        Me.rbPerPeriode.AutoSize = True
        Me.rbPerPeriode.Font = New System.Drawing.Font("Franklin Gothic Demi", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPerPeriode.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.rbPerPeriode.Location = New System.Drawing.Point(112, 183)
        Me.rbPerPeriode.Margin = New System.Windows.Forms.Padding(2)
        Me.rbPerPeriode.Name = "rbPerPeriode"
        Me.rbPerPeriode.Size = New System.Drawing.Size(97, 22)
        Me.rbPerPeriode.TabIndex = 8
        Me.rbPerPeriode.TabStop = True
        Me.rbPerPeriode.Text = "Per Periode"
        Me.rbPerPeriode.UseVisualStyleBackColor = True
        '
        'rbPerTanggal
        '
        Me.rbPerTanggal.AutoSize = True
        Me.rbPerTanggal.Font = New System.Drawing.Font("Franklin Gothic Demi", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPerTanggal.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.rbPerTanggal.Location = New System.Drawing.Point(112, 148)
        Me.rbPerTanggal.Margin = New System.Windows.Forms.Padding(2)
        Me.rbPerTanggal.Name = "rbPerTanggal"
        Me.rbPerTanggal.Size = New System.Drawing.Size(101, 22)
        Me.rbPerTanggal.TabIndex = 7
        Me.rbPerTanggal.TabStop = True
        Me.rbPerTanggal.Text = "Per Tanggal"
        Me.rbPerTanggal.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkSlateGray
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(184, 324)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(138, 41)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Cetak"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.BackColor = System.Drawing.Color.DarkSlateGray
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(390, 324)
        Me.Button2.Margin = New System.Windows.Forms.Padding(2)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(138, 41)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Keluar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.DarkSlateGray
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(679, 67)
        Me.Panel2.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Franklin Gothic Demi", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(208, 20)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(318, 30)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "LAPORAN DATA PELANGGAN"
        '
        'Frmlaporan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(676, 488)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Frmlaporan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtBulan As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtTahun As System.Windows.Forms.DateTimePicker
    Friend WithEvents rbPerBulan As System.Windows.Forms.RadioButton
    Friend WithEvents CboBulan As System.Windows.Forms.ComboBox
    Friend WithEvents dtPerPeriode2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtPerPeriode1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtPerTanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents rbPerTahun As System.Windows.Forms.RadioButton
    Friend WithEvents rbPerPeriode As System.Windows.Forms.RadioButton
    Friend WithEvents rbPerTanggal As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
