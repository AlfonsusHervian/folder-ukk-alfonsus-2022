﻿Imports System.Data.SqlClient
Public Class FrmFasilitasHotel
    Sub KondisiAwal()
        Call kodeotomatis()
        txtnama.Text = ""
        txtlantai.Text = ""
        txtketerangan.Text = ""
        txtnama.Enabled = False
        txtlantai.Enabled = False
        txtketerangan.Enabled = False
        Button1.Text = "Tambah"
        Button2.Text = "Ubah"
        Button3.Text = "Hapus"
        Button4.Text = "Tutup"
        Button4.Enabled = True
        Button3.Enabled = True
        Button2.Enabled = True
        Button1.Enabled = True
        Call Koneksi()
        Da = New SqlDataAdapter("select * from TBL_FASILITASHOTEl", Conn)
        Ds = New DataSet
        Da.Fill(Ds, "TBL_FASILITASHOTEl")
        DataGridView1.DataSource = (Ds.Tables("TBL_FASILITASHOTEl"))
        DataGridView1.Columns(0).HeaderText = "ID Fasilitas"
        DataGridView1.Columns(1).HeaderText = "Nama"
        DataGridView1.Columns(2).HeaderText = "Lantai"
        DataGridView1.Columns(3).HeaderText = "Keterangan"
        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
    End Sub

    Sub SiapIsi()
        txtnama.Enabled = True
        txtlantai.Enabled = True
        txtketerangan.Enabled = True
    End Sub

    Sub KunciHapus()
        txtnama.Enabled = False
        txtlantai.Enabled = False
        txtketerangan.Enabled = False
    End Sub

    Sub kodeotomatis()
        Call Koneksi()
        Cmd = New SqlCommand("select * from TBL_FASILITASHOTEl where IDFasilitas in (select max(IDFasilitas) from TBL_FASILITASHOTEl)", Conn)
        Dim UrutanKode As String
        Dim Hitung As Long
        Rd = cmd.ExecuteReader
        Rd.Read()
        If Not Rd.HasRows Then
            UrutanKode = "FH" + "001"
        Else
            Hitung = Microsoft.VisualBasic.Right(Rd.GetString(0), 3) + 1
            UrutanKode = "FH" + Microsoft.VisualBasic.Right("000" & Hitung, 3)
        End If
        txtid.Text = UrutanKode
    End Sub
    Private Sub FrmFasilitasHotel_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call KondisiAwal()
        Call kodeotomatis()
        txtid.Enabled = False
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Button1.Text = "Tambah" Then
            Button1.Text = "Simpan"
            Button2.Enabled = False
            Button3.Enabled = False
            Button4.Text = "Batal"
            Call SiapIsi()
        Else
            If txtnama.Text = "" Or txtlantai.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                Call Koneksi()
                Dim SimpanData As String = "insert into TBL_FASILITASHOTEL values('" & txtid.Text & "','" & txtnama.Text & "','" & txtlantai.Text & "','" & txtketerangan.Text & "')"
                Cmd = New SqlCommand(SimpanData, Conn)
                Cmd.ExecuteNonQuery()
                MsgBox("Data Berhasil Di Simpan", MsgBoxStyle.Information, "Hotel Emon")
                Call KondisiAwal()
            End If
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Button2.Text = "Ubah" Then
            Button2.Text = "Simpan"
            Button1.Enabled = False
            Button3.Enabled = False
            Button4.Text = "Batal"
            Call SiapIsi()
        Else
            If txtnama.Text = "" Or txtlantai.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                Call Koneksi()
                Dim EditData As String = "update TBL_FASILITASHOTEL set Nama='" & txtnama.Text & "',Lantai='" & txtlantai.Text & "',Keterangan='" & txtketerangan.Text & "' where IDFasilitas='" & txtid.Text & "'"
                Cmd = New SqlCommand(EditData, Conn)
                Cmd.ExecuteNonQuery()
                MsgBox("Data Berhasil Di Ubah", MsgBoxStyle.Information, "Hotel Emon")
                Call KondisiAwal()
            End If
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If Button3.Text = "Hapus" Then
            Button3.Text = "hapus"
            Button1.Enabled = False
            Button2.Enabled = False
            Button4.Text = "Batal"
            Call KunciHapus()
        Else
            If txtnama.Text = "" Or txtlantai.Text = "" Then
                MsgBox("Pastikan Semua Data Telah Di Isi", MsgBoxStyle.Information, "Hotel Emon")
            Else
                If MessageBox.Show("Apakah Anda Ingin Menghapus Data ?", "Hotel Emon", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                    Call Koneksi()
                    Dim HapusData As String = "delete TBL_FASILITASHOTEL where IDFasilitas='" & txtid.Text & "'"
                    Cmd = New SqlCommand(HapusData, Conn)
                    Cmd.ExecuteNonQuery()
                    MsgBox("Data Berhasil Di Hapus", MsgBoxStyle.Information, "Hotel Emon")
                    Call KondisiAwal()
                Else
                End If
            End If
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Button4.Text = "Tutup" Then
            Me.Close()
        Else
            Call KondisiAwal()
        End If
    End Sub

    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        If Button2.Text = "Simpan" Then
            Dim i As Integer
            i = DataGridView1.CurrentRow.Index
            On Error Resume Next
            txtid.Text = DataGridView1.Item(0, i).Value
            txtnama.Text = DataGridView1.Item(1, i).Value
            txtlantai.Text = DataGridView1.Item(2, i).Value
            txtketerangan.Text = DataGridView1.Item(3, i).Value
        Else
            If Button3.Text = "hapus" Then
                Dim a As Integer
                a = DataGridView1.CurrentRow.Index
                On Error Resume Next
                txtid.Text = DataGridView1.Item(0, a).Value
                txtnama.Text = DataGridView1.Item(1, a).Value
                txtlantai.Text = DataGridView1.Item(2, a).Value
                txtketerangan.Text = DataGridView1.Item(3, a).Value
            Else
            End If
        End If
    End Sub
End Class