﻿Imports CrystalDecisions.CrystalReports.Engine
Imports Microsoft.VisualBasic
Public Class FrmMenuUtama

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles Pengguna.Click
        FrmPengguna.ShowDialog()
    End Sub

    Private Sub FrmMenuUtama_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label4.Text = Today
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Label6.Text = TimeOfDay
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        If MsgBox("Anda yakin ingin Keluar ?", MsgBoxStyle.Information Or MsgBoxStyle.YesNo, "Hotel Emon") = MsgBoxResult.Yes Then
            FrmLogin.Show()
            Me.Hide()
        Else
        End If
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles Kamar.Click
        FrmKamar.ShowDialog()
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles Tamu.Click
        FrmTamu.ShowDialog()
    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles Checkin.Click
        TampilDataCekIn.ShowDialog()
    End Sub

    Private Sub PictureBox8_Click(sender As Object, e As EventArgs) Handles Checkout.Click
        TampilDataCekOut.ShowDialog()
    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles Hotel.Click
        FrmFasilitasHotel.ShowDialog()
    End Sub

    Private Sub PictureBox9_Click(sender As Object, e As EventArgs) Handles Laporan.Click
        Frmlaporan.ShowDialog()
    End Sub

    Private Sub Pengguna_MouseEnter(sender As Object, e As EventArgs) Handles Pengguna.MouseEnter
        Me.Pengguna.Image = My.Resources.Pengguna_1
    End Sub

    Private Sub Pengguna_MouseLeave(sender As Object, e As EventArgs) Handles Pengguna.MouseLeave
        Me.Pengguna.Image = My.Resources.icons8_user_48
    End Sub

    Private Sub Tamu_MouseEnter(sender As Object, e As EventArgs) Handles Tamu.MouseEnter
        Me.Tamu.Image = My.Resources.Tamu1
    End Sub

    Private Sub Tamu_MouseLeave(sender As Object, e As EventArgs) Handles Tamu.MouseLeave
        Me.Tamu.Image = My.Resources.icons8_customer_64
    End Sub

    Private Sub Kamar_MouseEnter(sender As Object, e As EventArgs) Handles Kamar.MouseEnter
        Me.Kamar.Image = My.Resources.Kamar1
    End Sub

    Private Sub Kamar_MouseLeave(sender As Object, e As EventArgs) Handles Kamar.MouseLeave
        Me.Kamar.Image = My.Resources.icons8_living_room_64
    End Sub

    Private Sub Hotel_MouseEnter(sender As Object, e As EventArgs) Handles Hotel.MouseEnter
        Me.Hotel.Image = My.Resources.Hotel1
    End Sub

    Private Sub Hotel_MouseLeave(sender As Object, e As EventArgs) Handles Hotel.MouseLeave
        Me.Hotel.Image = My.Resources.icons8_hotel_641
    End Sub

    Private Sub Checkin_MouseEnter(sender As Object, e As EventArgs) Handles Checkin.MouseEnter
        Me.Checkin.Image = My.Resources.Checkin1
    End Sub

    Private Sub Checkin_MouseLeave(sender As Object, e As EventArgs) Handles Checkin.MouseLeave
        Me.Checkin.Image = My.Resources.icons8_hotel_64__2_
    End Sub

    Private Sub Checkout_MouseEnter(sender As Object, e As EventArgs) Handles Checkout.MouseEnter
        Me.Checkout.Image = My.Resources.Checkout1
    End Sub

    Private Sub Checkout_MouseLeave(sender As Object, e As EventArgs) Handles Checkout.MouseLeave
        Me.Checkout.Image = My.Resources.icons8_hotel_64__3_
    End Sub

    Private Sub PictureBox4_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox4.MouseEnter
        Me.PictureBox4.Image = My.Resources.Logout1
    End Sub

    Private Sub PictureBox4_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox4.MouseLeave
        Me.PictureBox4.Image = My.Resources.icons8_lock_48
    End Sub

    Private Sub Panel2_Paint(sender As Object, e As PaintEventArgs) Handles Panel2.Paint

    End Sub
End Class